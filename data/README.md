# datasets

Training datasets are provided with ground truth in CSV format. There are ten training datasets numbered `1-10`, and five testing datasets numbered `1-5`. For each one there is a `calcium` file with calcium flouresence signals, and a `spikes` file with spike rates, both sampled at a common rate of `100 Hz`. The columns of each table are neurons, and the rows are time points. In a given dataset, some neurons will have slightly different numbers of time points than others, this is expected.

